package com.elavon.jtutorial.main;

import com.elavon.jtutorial.impl.ImplClass;

public class Main {

	public static void main(String[] args) {
		
		ImplClass i = new ImplClass();
		System.out.println(i.getSum(2,2));
		System.out.println(i.getDifference(23, 10));
		System.out.println(i.getProduct(5, 3));
		System.out.println(i.getQuotientAndRemainder(17, 5));
		System.out.println(i.toCelsius(32)+" �C");
		System.out.println(i.toFahrenheit(0)+" �F");
		System.out.println(i.toKilogram(5)+" kg");
		System.out.println(i.toPound(55)+" lbs");
		System.out.println(i.isPalindrome("radar"));
		
	}

}
