package com.elavon.jtutorial.exercise2;

public class ColoredTV extends TV {

	protected Integer brightness = 50;
	protected Integer contrast = 50;
	protected Integer picture = 50;

	public ColoredTV(String brand, String model) {

		super(brand, model);

	}

	public static void main(String[] args) {

//		TV sharp = new ColoredTV("Sharp", "C1");
//
//		System.out.println(sharp);

		TV sonyTV, bnwTV;

		bnwTV = new TV("Admiral", "A1");

		sonyTV = new ColoredTV("SONY", "S1");

		System.out.println(bnwTV);

		System.out.println(sonyTV);

		// Output of both bntTV and sonyTV reflects the TV functions with the additional properties on sonyTV

		// sonyTV.brightnessUP(); Method undefined. The method can't be used since the sonyTV is declared as a TV Object. It can use the default values of ColoredTV but can't use its methods

		ColoredTV sharpTV = new ColoredTV("SHARP", "SH1");

		sharpTV.mute();

		sharpTV.brightnessUp();
		sharpTV.contrastDown();
		sharpTV.pictureUp();

		sharpTV.switchToChannel(5);

		System.out.println(sharpTV);

	}

	protected void brightnessUp() {
		brightness = brightness + 1;

	}

	protected void brightnessDown() {
		brightness = brightness - 1;

	}

	protected void contrastUp() {
		contrast = contrast + 1;

	}

	protected void contrastDown() {
		contrast = contrast - 1;

	}

	protected void pictureUp() {
		picture = picture + 1;

	}

	protected void pictureDown() {
		picture = picture - 1;
	}

	protected void switchToChannel(int channel) {
		channel = channel;

	}

	protected void mute() {
		volume = 0;

	}

	public String toString() {
		return this.brand + " " + this.model + " [on:" + this.powerOn
				+ ", channel:" + this.channel + ", volume:" + this.volume
				+ "] " + "[b:" + this.brightness + ", c:" + this.contrast
				+ ", p:" + this.picture + "]";

	}

}