package com.elavon.jtutorial.exercise2;

public class TV {

	protected String brand;
	protected String model;
	protected Boolean powerOn = false;
	protected Integer channel = 0;
	protected Integer volume = 5;

	TV(String brand, String model) {

		this.brand = brand;
		this.model = model;
	}

	public static void main(String[] args) {

		TV tv1 = new TV("Sony", "Bravia");

		tv1.powerOn = true;
		tv1.channel = 0;
		tv1.volume = 5;

		System.out.println(tv1);

		TV tv2 = new TV("Andre Electronics", "ONE");

		tv2.channelUp();
		tv2.channelUp();
		tv2.channelUp();
		tv2.channelUp();
		tv2.channelUp();
		tv2.channelDown();
		tv2.volumeDown();
		tv2.volumeDown();
		tv2.volumeDown();
		tv2.volumeUp();

		tv2.powerOn = false;

		System.out.println(tv2);

	}

	public void turnOn() {
		powerOn = true;

	}

	public void turnOff() {
		powerOn = false;

	}

	public void channelUp() {
		channel = channel + 1;

	}

	public void channelDown() {
		channel = channel - 1;

	}

	public void volumeUp() {
		volume = volume + 1;

	}

	public void volumeDown() {
		volume = volume - 1;

	}

	public String toString() {
		return this.brand + " " + this.model + " [on:" + this.powerOn
				+ ", channel:" + this.channel + ", volume:" + this.volume + "]";

	}

}