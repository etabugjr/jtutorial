package com.elavon.jtutorial.exercise4;

public class TwelveDaysOfChristmas {

	public static void main(String[] args) {
		TwelveDaysOfChristmas.printLyrics();
	}

	public static void printLyrics() {
		int daynum = 1;
		String daydesc = null;
		while (daynum < 13) {
			switch (daynum) {
			case 1:
				daydesc = "first";
				break;
			case 2:
				daydesc = "second";
				break;
			case 3:
				daydesc = "third";
				break;
			case 4:
				daydesc = "fourth";
				break;
			case 5:
				daydesc = "fifth";
				break;
			case 6:
				daydesc = "sixth";
				break;
			case 7:
				daydesc = "seventh";
				break;
			case 8:
				daydesc = "eighth";
				break;
			case 9:
				daydesc = "ninth";
				break;
			case 10:
				daydesc = "tenth";
				break;
			case 11:
				daydesc = "eleventh";
				break;
			case 12:
				daydesc = "twelfth";
				break;
			}
			System.out.println("On the " + daydesc
					+ " day of Christmas, My true love gave to me");
			switch (daynum) {
			case 12:
				System.out.println("Twelve Drummers Drumming");
			case 11:
				System.out.println("Eleven Pipers Piping");
			case 10:
				System.out.println("Ten Lords-A-Leaping");
			case 9:
				System.out.println("Nine Ladies Dancing");
			case 8:
				System.out.println("Eight Maids-A-Milking");
			case 7:
				System.out.println("Seven Swans-A-Swimming");
			case 6:
				System.out.println("Six Geese-A-Laying");
			case 5:
				System.out.println("Five Golden Rings");
			case 4:
				System.out.println("Four Calling Birds");
			case 3:
				System.out.println("Three French Hens");
			case 2:
				System.out.println("Two Turtle Doves");
			case 1:
				if (daynum == 1) {
					System.out.println("A Partridge in a Pear Tree");
					System.out.println("");
				} else {
					System.out.println("And Partridge in a Pear Tree");
					System.out.println("");
					break;
				}
			}
			daynum++;
		}

	}
}