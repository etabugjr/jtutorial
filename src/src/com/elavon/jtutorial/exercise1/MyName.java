package com.elavon.jtutorial.exercise1;

public class MyName {
		public static void main(String[] args) {
			System.out.println("***** *     *   * *****");
			System.out.println("*     *     ** ** *   *");
			System.out.println("***** *     * * * *   *");
			System.out.println("*     *     *   * *   *");
			System.out.println("***** ***** *   * *****");
			System.out.println();
			System.out.println("*****   *   ****   *   * *****");
			System.out.println("  *    * *  *   *  *   * *    ");
			System.out.println("  *   ***** ****   *   * * ***");
			System.out.println("  *   *   * *   *  *   * *   *");
			System.out.println("  *   *   * ****    ***   ****");
			System.out.println();
			System.out.println("    * ****");
			System.out.println("    * *   *");
			System.out.println("*   * ****");
			System.out.println("*   * *   *");
			System.out.println(" ***  *   *");
		}
}
