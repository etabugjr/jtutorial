package com.elavon.jtutorial.impl;

import com.elavon.jtutorial.intf.AwesomeCalculator;

public class ImplClass implements AwesomeCalculator {
	
	public int getSum(int augend, int addend) {
		System.out.print("Sum of " + augend + " and " + addend + " is ");
		return augend + addend;
	}

	public double getDifference(double minuend, double subtrahend) {
		System.out.print("Difference of " + minuend + " and " + subtrahend + " is ");
		return minuend - subtrahend;
	}

	public double getProduct(double multiplicand, double multiplier) {
		System.out.print("Product of " + multiplicand + " and " + multiplier + " is ");
		return multiplicand * multiplier;
	}

	public String getQuotientAndRemainder(double dividend, double divisor) {
		System.out.print("Quotient & Remainder of " + dividend + " and " + divisor + " is ");
		return "Quotient: " + (int)(dividend/divisor) + " Remainder: " + (int)(dividend%divisor);
	}
	
	public double toCelsius(int fahrenheit) {
		System.out.print("Celsius conversion of " + fahrenheit + " �F is ");
		return (fahrenheit - 32) / 1.8;
	}

	public double toFahrenheit(int celsius) {
		System.out.print("Fahrenheit conversion of " + celsius + " �C is ");
		return (celsius * 1.8) + 32;
	}

	public double toKilogram(double lbs) {
		System.out.print("Kilogram conversion of " + lbs + " lbs is ");
		return lbs * 0.454;
	}

	public double toPound(double kg) {
		System.out.print("Pound conversion of " + kg + " kg is ");
		return kg * 2.2;
	}

	public boolean isPalindrome(String str) {
		String reverse = "";
		int lenght = str.length();
		
		for (int i=lenght-1; i>=0; i--)
			reverse = reverse + str.charAt(i);
		
		System.out.print("Palindrome of " + str.toLowerCase() + " and " + reverse.toLowerCase() + " is ");
		if (str.toLowerCase().equals(reverse.toLowerCase()))
			return true;
		else
			return false;
	}

}
